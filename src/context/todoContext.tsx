import { useState, useEffect, createContext } from "react";
import { FilterType, InputType, TodoContextType } from "../typings";
const inputDefault = {
  titulo: "",
  descripcion: "",
  fecha: new Date(),
  favorito: false,
  terminada: false,
};
export const TodoContext = createContext<TodoContextType>({
  filter: "todas",
  setFilter: () => null,
  handleSwitch: () => null,
  data: [inputDefault],
  input: inputDefault,
  handleInputs: (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => null,
  handleForm: (e: React.FormEvent<HTMLFormElement>) => null,
  handleAddToFavorites: (e: number) => null,
  handleDelete: (e: number) => null,
  handleDone: (index: number) => null,
});

interface Props {
  children: JSX.Element | JSX.Element[];
}

export const TodoProvider = ({ children }: Props) => {
  const [data, setData] = useState<Array<InputType>>([]);
  const [filter, setFilter] = useState<FilterType>("todas");
  const [input, setInput] = useState<InputType>(inputDefault);

  const getTodos = () => {
    fetch("https://my-json-server.typicode.com/adriangarb/todo-app-api/data")
      .then((response) => response.json())
      .then((json) => setData(json));
  };

  const handleSwitch = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    setInput({ ...input, favorito: !input.favorito });
  };

  const handleInputs = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setInput({ ...input, [e.target.name]: e.target.value });
  };

  const handleForm = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    fetch("https://my-json-server.typicode.com/adriangarb/todo-app-api/data", {
      method: "POST",
      body: JSON.stringify(input),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    })
    .then((response) => response.json())
    .then((json) => setData((prevState) => prevState.concat(input)));
    setInput(inputDefault);
  };

  const handleDelete = (index: number) => {
    const newData: InputType[] = [...data];
    newData.splice(index, 1);
    setData(newData)
    fetch("https://my-json-server.typicode.com/adriangarb/todo-app-api/data", {
      method: "POST",
      body: JSON.stringify(newData),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    })
  };

  const handleAddToFavorites = (index: number) => {
    const newData: InputType[] = [...data];
    newData[index].favorito = !newData[index].favorito;
    setData(newData)
    fetch("https://my-json-server.typicode.com/adriangarb/todo-app-api/data", {
      method: "POST",
      body: JSON.stringify(newData),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    });
  };

  const handleDone = (index: number) => {
    const newData: InputType[] = [...data];
    newData[index].terminada = !newData[index].terminada;
    setData(newData)
    fetch("https://my-json-server.typicode.com/adriangarb/todo-app-api/data", {
      method: "POST",
      body: JSON.stringify(newData),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    });
  };

  useEffect(() => {
    getTodos();
  }, []);

  return (
    <TodoContext.Provider
      value={{
        filter,
        setFilter,
        handleSwitch,
        data,
        input,
        handleInputs,
        handleForm,
        handleAddToFavorites,
        handleDelete,
        handleDone,
      }}
    >
      {children}
    </TodoContext.Provider>
  );
};
