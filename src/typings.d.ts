export type FilterType = "todas" | "terminadas" | "favoritas";
export type InputType = {
    titulo: string,
    descripcion: string,
    fecha: null | Date,
    favorito: boolean,
    terminada:boolean
}
export type TodoContextType = {
    filter: FilterType;
    setFilter: React.Dispatch<React.SetStateAction<FilterType>>;
    handleSwitch: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
    data: InputType[];
    input: InputType;
    handleInputs: (
      e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
    ) => void;
    handleForm: (e: React.FormEvent<HTMLFormElement>) => void;
    handleAddToFavorites: (e: number) => void;
    handleDelete: (e: number) => void;
    handleDone: (index: number)=>void
  };