import React, { useContext } from "react";
import TodoCard from "./components/TodoCard";
import { TextField, Switch, Button } from "@mui/material";
import "./styles/App.sass";
import { TodoContext } from "./context/todoContext";
function App() {
  const { handleSwitch, data, input, handleInputs, handleForm, filter } =
    useContext(TodoContext);
  return (
    <div className="App">
      <form onSubmit={handleForm} className="App_form">
        <div className="App_form_inputs">
          <TextField
            name="titulo"
            value={input.titulo}
            onChange={handleInputs}
            color="info"
            className="App_form_inputs_input"
            id="outlined-basic"
            label="Titulo"
            variant="outlined"
          />
          <TextField
            name="descripcion"
            value={input.descripcion}
            onChange={handleInputs}
            color="info"
            className="App_form_inputs_input"
            id="outlined-basic"
            label="Descripcion"
            variant="outlined"
          />
        </div>
        <div>
          <span>
            Añadir a favoritos:{" "}
            <Switch checked={input.favorito} onClick={handleSwitch} />
          </span>
          <Button type="submit" variant="contained">
            Añadir Nota
          </Button>
        </div>
      </form>
      <div className="App_notes">
        {filter === "todas" && data.length < 1 && <span>Aun no hay notas</span>}
        {filter === "todas" &&
          data.map((item, index) => (
            <TodoCard
              key={index}
              titulo={item.titulo}
              descripcion={item.descripcion}
              favorito={item.favorito}
              index={index}
            />
          ))}
        {filter === "terminadas" &&
          data.filter((item) => item.terminada === true).length < 1 && (
            <span>Aun no hay notas terminadas</span>
          )}
        {filter === "terminadas" &&
          data
            .filter((item) => item.terminada === true)
            .map((item, index) => (
              <TodoCard
                key={index}
                titulo={item.titulo}
                descripcion={item.descripcion}
                favorito={item.favorito}
                index={index}
              />
            ))}
        {filter === "favoritas" &&
          data.filter((item) => item.terminada === true).length < 1 && (
            <span>Aun no hay notas favoritas</span>
          )}
        {filter === "favoritas" &&
          data
            .filter((item) => item.favorito === true)
            .map((item, index) => (
              <TodoCard
                key={index}
                titulo={item.titulo}
                descripcion={item.descripcion}
                favorito={item.favorito}
                index={index}
              />
            ))}
      </div>
    </div>
  );
}

export default App;
