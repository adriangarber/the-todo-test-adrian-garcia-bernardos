import { Button, Card, CardActions, CardContent, Checkbox } from '@mui/material'
import React,{useContext} from 'react'
import '../styles/Card.sass'
import StarIcon from '@mui/icons-material/Star';
import StarBorderIcon from '@mui/icons-material/StarBorder';
import { TodoContext } from '../context/todoContext';
interface Props {
    titulo: string
    descripcion: string
    favorito: boolean
    index: number
}
const TodoCard:React.FC<Props> = ({titulo,descripcion,favorito,index}) => {
  const {handleDelete,handleAddToFavorites,handleDone} = useContext(TodoContext)
  return (
    <Card className='Card' >
      <Checkbox onClick={()=>handleDone(index)}/>
      <CardContent>
        <h3>{titulo}</h3>
        <p>{descripcion}</p>
      </CardContent>
      <CardActions>
        <Button onClick={()=>handleAddToFavorites(index)}>{favorito ? 'Quitar de' : 'Añadir a'} favoritos</Button>
        <Button onClick={()=>handleDelete(index)}>Eliminar</Button>
      </CardActions>
      {favorito ? <StarIcon/> : <StarBorderIcon/>}
    </Card>
  )
}

export default TodoCard