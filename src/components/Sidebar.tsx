import React, {useContext} from 'react'
import { TodoContext } from '../context/todoContext'
import '../styles/Sidebar.sass'
import {Card} from '@mui/material'
import {FilterType} from '../typings'
function Sidebar() {
  const {setFilter} = useContext(TodoContext)
  const changeActive = (e:React.MouseEvent<HTMLButtonElement, MouseEvent>) =>{
    const paragraps = document.body.querySelectorAll('.paragraph');
    paragraps.forEach(item=>item.classList.remove('active'));
    e.currentTarget.classList.toggle('active');
    const name = e.currentTarget.name
    setFilter(name as FilterType)
  }
  return (
    <Card className='Sidebar'>
        <button onClick={changeActive} name="todas" className='active paragraph'>Todas las notas</button>
        <button onClick={changeActive} name="terminadas" className='paragraph'>Terminadas</button>
        <button onClick={changeActive} name="favoritas" className='paragraph'>Favoritas</button>
    </Card>
  )
}

export default Sidebar