import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import Sidebar from './components/Sidebar';
import { TodoProvider } from './context/todoContext';
import reportWebVitals from './reportWebVitals';
import './styles/globals.sass'
const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <TodoProvider>
    <React.StrictMode>
      <Sidebar/>
      <App />
   </React.StrictMode>
  </TodoProvider>
  
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
